#include <SparkFunTSL2561.h>
#include <OpenLog.h>

SFE_TSL2561 light;
OpenLog openLog;

boolean gain;     // Gain setting, 0 = X1, 1 = X16;
unsigned int ms;  // Integration ("shutter") time in milliseconds

unsigned long lastTs;
int MAX_LUMEN = 10;
int MIN_LUMEN = 2;
boolean makeNewMeasure = true;

int ledPin = D7;

void setup() {

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(2000);
  digitalWrite(ledPin, LOW);
  delay(2000);


  // Initialize the Serial port:
  Serial.begin(9600);
  Serial.println("PowerMeter");

  openLog.init();

  light.begin();

  // If gain = false (0), device is set to low gain (1X)
  // If gain = high (1), device is set to high gain (16X)
  gain = 1;

  // If time = 0, integration will be 13.7ms
  // If time = 1, integration will be 101ms
  // If time = 2, integration will be 402ms
  // If time = 3, use manual start / stop to perform your own integration
  unsigned char time = 1;

  Serial.println("Set timing...");
  light.setTiming(gain,time,ms);

  Serial.println("Powerup...");
  light.setPowerUp();
  lastTs = millis();
}

void loop() {
  //digitalWrite(ledPin, HIGH);
  delay(ms);
  //digitalWrite(ledPin, LOW);

  unsigned int data0, data1;

  if (light.getData(data0, data1)) {

    double lux; // Resulting lux value

    light.getLux(gain,ms,data0,data1,lux);

    // Print out the results:
    if (lux > MAX_LUMEN && makeNewMeasure) {
        printMeasurement(lux);
        makeNewMeasure = false;
        digitalWrite(ledPin, HIGH);
    } else if (lux < MIN_LUMEN) {
        makeNewMeasure = true;
        digitalWrite(ledPin, LOW);
    }
  }
  else {
    byte error = light.getError();
    printError(error);
  }
}

void printMeasurement(double lux) {
  long ts = millis();
  float deltaTs = (float)(ts - lastTs)/1000.0f;
  float kWh = 36.0f/(deltaTs*10.0f);

  String data = String("deltaTs: " + String(deltaTs, DEC));
  data.concat(" kWh: " + String(kWh));
  data.concat(" Lux: " + String(lux));
  Serial.println(data);
  openLog.write(deltaTs, kWh);
  lastTs = ts;
}

void printError(byte error) {
  Serial.print("I2C error: ");
  Serial.print(error,DEC);
  Serial.print(", ");

  switch(error) {
    case 0:
      Serial.println("success");
      break;
    case 1:
      Serial.println("data too long for transmit buffer");
      break;
    case 2:
      Serial.println("received NACK on address (disconnected?)");
      break;
    case 3:
      Serial.println("received NACK on data");
      break;
    case 4:
      Serial.println("other error");
      break;
    default:
      Serial.println("unknown error");
  }
}
