# PowerMeter - README #

"Powermeter" is a simple application to measure the power consumption.
Many current meters have a status LED, which indicates the current power consumption. As a rule, the status LED flashes with 1000 pulses / kWh

"Powermeter" measures the time period of the flashing status LED and calculates the current power consumption. The data is stored on an SD card.


### Hardware requirements ###

- Particle Photon - https://www.particle.io/products/hardware/photon-wifi-dev-kit
- Grove Digital Light Sensor - http://wiki.seeed.cc/Grove-Digital_Light_Sensor/
- SparkFun OpenLog - https://www.sparkfun.com/products/13712

### Software requirements ###

- Atom IDE - https://atom.io


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Bosshard Patrick