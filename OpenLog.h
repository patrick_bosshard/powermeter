#ifndef OpenLog_h
#define OpenLog_h

#include "application.h"

class OpenLog
{
	public:
		OpenLog(void);
		void init(void);
    void write(float deltaTs, float kWh);
};

#endif
