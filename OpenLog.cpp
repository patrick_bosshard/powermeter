#include <OpenLog.h>

OpenLog::OpenLog() {
}

void OpenLog::init() {
  Serial1.begin(9600);
  String header = String("Timestamp;deltaTs;kWh");
  Serial1.println(header);
}

void OpenLog::write(float deltaTs, float kWh) {
  String ts = String(Time.format(Time.now(), TIME_FORMAT_ISO8601_FULL));
  Serial1.println(ts + ";" + String(deltaTs, DEC) + ";" + String(kWh, DEC));
  delay(15); // let some time to write to sd card
}
